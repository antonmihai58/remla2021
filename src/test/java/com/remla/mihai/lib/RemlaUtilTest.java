package com.remla.mihai.lib;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;


public class RemlaUtilTest {
    
    @Test
    public void hostNameNotNullOrEmtpy() {
        String actual = RemlaUtil.getHostName();
        assertNotNull(actual);
    }

    @Test
    public void versionNotNullOrEmpty(){
        String actual = RemlaUtil.getUtilVersion();
        assertNotNull(actual);
    }
}
