package com.remla.mihai;


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.printf( "Hostname: %s\n", com.remla.mihai.lib.RemlaUtil.getHostName() );
        System.out.printf( "Version: %s\n", com.remla.mihai.lib.RemlaUtil.getUtilVersion() );
    }
}
